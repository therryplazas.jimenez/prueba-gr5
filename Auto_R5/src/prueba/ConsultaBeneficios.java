package prueba;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class ConsultaBeneficios {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "\\chromedriver.exe");
		String url = "Agregar direccion de la carpeta\\Evidencia";
		
		WebDriver d = new ChromeDriver();

		File capturaP = ((TakesScreenshot) d).getScreenshotAs(OutputType.FILE);
		d.manage().window().maximize();
		d.get("https://soat.segurosfalabella.com.co/consult-gift");
		
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-1" + ".png"));

		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("dghsjgfdjhgfjhsdgf");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-2" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).clear();

		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("56456456465");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-3" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).clear();

		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("!#%$#&%&$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-4" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).clear();

		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("mms41c");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-6" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).clear();

		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("dghsjgfdjhgfjhsdgf");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-7" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("56456456465");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-8" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("!#%$#&%&$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-9" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("1023856984");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-10" + ".png"));

		d.findElement(By.xpath(
				"/html/body/app-root/app-consult-gift/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[4]/div[2]/app-gift-form/form/div[4]/button"))
				.click();
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-1\"]/label/div/div/div[1]")).click();
		d.findElement(By.xpath(
				"/html/body/app-root/app-consult-gift/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[4]/div[2]/app-gift-form/form/div[4]/button"))
				.click();

		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "ConsulBeneficios-11" + ".png"));
	}

}
