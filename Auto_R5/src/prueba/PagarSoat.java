package prueba;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class PagarSoat {
	
	public static void main(String[] args) throws IOException, InterruptedException {
		System.setProperty("webdriver.chrome.driver", "\\chromedriver.exe");
		String url = "Agregar direccion de la carpeta\\Evidencia";

		WebDriver d = new ChromeDriver();

		File capturaP = ((TakesScreenshot) d).getScreenshotAs(OutputType.FILE);
		d.manage().window().maximize();
		d.get("https://soat.segurosfalabella.com.co/sale/step0");
		
//		//Consulta placa incorrecta (Solo Texto)
//		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
//		FileUtils.copyFile(capturaP, new File(url + "SoatConsulta-1" + ".png"));
//		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("wertyuio");
//		d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[1]/div/app-quotation-form/div/div[4]/button")).click();
//		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
//		FileUtils.copyFile(capturaP, new File(url + "SoatConsulta-2" + ".png"));
//		d.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-error/div/div[2]/div/button/span")).click();
//		
//		//Consulta placa incorrecta (Solo Número)
//		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
//		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("1234567");
//		d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[1]/div/app-quotation-form/div/div[4]/button")).click();
//		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
//		FileUtils.copyFile(capturaP, new File(url + "SoatConsulta-3" + ".png"));
//		d.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-error/div/div[2]/div/button/span")).click();
//
//
//		//Consulta placa incorrecta (Solo Caracteres especiales)
//		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
//		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("!#$%&/=?¡");
//		d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[1]/div/app-quotation-form/div/div[4]/button")).click();
//		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
//		FileUtils.copyFile(capturaP, new File(url + "SoatConsulta-4" + ".png"));
//		d.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-error/div/div[2]/div/button")).click();
	
		
		//Consulta placa existente
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("mms41c");
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-1\"]")).click();
		Thread.sleep(4000);
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-1\"]")).click();
		d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[1]/div/app-quotation-form/div/div[4]/button")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "SoatConsulta-5" + ".png"));
//		d.findElement(By.xpath("/html/body/div[2]/div[2]/div/mat-dialog-container/app-error/div/div[2]/div/button")).click();
//		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-1\"]/label/div")).click();
//		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
//		FileUtils.copyFile(capturaP, new File(url + "SoatConsulta-6" + ".png"));
//		d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[1]/div/app-quotation-form/div/div[4]/button/span")).click();

		
		//Beneficios
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Beneficios-1" + ".png"));
		d.findElement(By.xpath("//*[@id=\"cdk-accordion-child-12\"]/div/div/app-gift-chooser/div/div[2]/div/div[3]/button/span")).click();

		
		//Completar Información Personal
		d.findElement(By.xpath("//*[@id=\"cdk-accordion-child-10\"]/div/div/div[5]/div[1]/mat-form-field/div/div[1]/div[3]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-datepicker-0\"]/div/mat-month-view/table/tbody/tr[3]/td[5]/div")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-1" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-select-0\"]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-option-1\"]")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-2" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).sendKeys("1111111111111111111111111111111111111111111111111111111111111111111");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-3" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).sendKeys("Cra 60# 43 - 900");;
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-4" + ".png"));
		
		
		for (int i = 0; i < 125; i++) {
			d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/section/div/section/div/app-vehicle-form/form/mat-accordion/mat-expansion-panel[2]/div/div/div/div[7]/div[1]/mat-form-field/div/div[1]/div[3]/input")).sendKeys("abcde");
		}
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-5" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-4\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).sendKeys("fgshjdgfhjsgdjhfgshdhjdsgfjsdgfgdsjgfjsd");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-6" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).sendKeys("123123123213213213728163872163872168321");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-7" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).sendKeys("!#$%$#%#$%$#%$#%#$%#$%#$%#$%#$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-8" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).clear();
		d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/section/div/section/div/app-vehicle-form/form/mat-accordion/mat-expansion-panel[2]/div/div/div/div[7]/div[1]/mat-form-field/div/div[1]/div[3]/input")).sendKeys("prueba@gmail.com");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-9" + ".png"));
		
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).sendKeys("fgshjdgfhjsgdjhfgshdhjdsgfjsdgfgdsjgfjsd");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-10" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).sendKeys("123123123213213213728163872163872168321");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-11" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).sendKeys("!#$%$#%#$%$#%$#%#$%#$%#$%#$%#$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-12" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-5\"]")).sendKeys("302285449800");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-13" + ".png"));
		
		
		d.findElement(By.xpath("/html/body/app-root/app-sale/app-step0/section/div/section/div/app-vehicle-form/form/div[2]/button/span")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "InfoPersonal-14" + ".png"));
		
		//Pago
		d.findElement(By.xpath("//*[@id=\"mat-select-1\"]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-option-78\"]/span")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-1" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-select-2\"]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-option-3\"]/span")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-2" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-tab-content-2-0\"]/div/app-pse-form/form/div[5]/div/div/button[2]/span")).click();
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-2\"]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-2\"]")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-3" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-tab-label-2-1\"]")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-4" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-tab-content-2-1\"]/div/app-cash/form/div[3]/div/div/button[2]/span")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-5" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-3\"]/label")).click();
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-3\"]/label")).click();
		d.findElement(By.xpath("//*[@id=\"mat-tab-label-2-2\"]")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-6" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-6\"]")).sendKeys("1658919848916185");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-7" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-6\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-6\"]")).sendKeys("/////////////////////////////////////////////////////////////////");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-8" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-6\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-6\"]")).sendKeys("dgfdgsdfgdsfgdsfdgfdgsdfgdsfgdsf");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-9" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-select-3\"]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-option-7\"]")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-10" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-select-4\"]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-option-27\"]")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-11" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-8\"]")).sendKeys("dgfdgsdfgdsfgdsfdgfdgsdfgdsfgdsf");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-12" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-8\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-8\"]")).sendKeys("/////////////////////////////////////////////////////////////////");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-13" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-8\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-8\"]")).sendKeys("12345");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-14" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-select-5\"]")).click();
		d.findElement(By.xpath("//*[@id=\"mat-option-63\"]")).click();
		//*[@id="mat-input-6"]
		d.findElement(By.xpath("//*[@id=\"mat-tab-content-2-2\"]/div/app-credit-card-form/form/div[6]/div/div/button[2]/span")).click();
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-4\"]/label")).click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Pago-15" + ".png"));
	}
}