package prueba;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class consultaSoat {

	public static void main(String[] args) throws IOException {
		System.setProperty("webdriver.chrome.driver", "\\chromedriver.exe");
		String url = "Agregar direccion de la carpeta\\Evidencia";

		WebDriver d = new ChromeDriver();

		File capturaP = ((TakesScreenshot) d).getScreenshotAs(OutputType.FILE);
		d.manage().window().maximize();
		d.get("https://soat.segurosfalabella.com.co/consult-expiration");

		// Ingresar Placa
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("fgshjdgfhjsgdjhfgshdhjdsgfjsdgfgdsjgfjsd");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-1" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("123123123213213213728163872163872168321");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-2" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("!#$%$#%#$%$#%$#%#$%#$%#$%#$%#$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-3" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-0\"]")).sendKeys("MMS41C");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-4" + ".png"));
		// Nombre Completo
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("fgshjdgfhjsgdjhfgshdhjdsgfjsdgfgdsjgfjsd");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-5" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("123123123213213213728163872163872168321");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-6" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("!#$%$#%#$%$#%$#%#$%#$%#$%#$%#$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-7" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-1\"]")).sendKeys("EDISON GAMBOA MAHECHA");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-8" + ".png"));

		// Correo Electronico
		for (int i = 0; i < 125; i++) {
			d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("abcde");
		}
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-9" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("fgshjdgfhjsgdjhfgshdhjdsgfjsdgfgdsjgfjsd");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-10" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("123123123213213213728163872163872168321");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-11" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys(".!#$%$#%#$%$#%$#%#$%#$%#$%#$%#$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-12" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-2\"]")).sendKeys("prueba@gmail.com");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-13" + ".png"));

		// N�mero de Celular
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).sendKeys("fgshjdgfhjsgdjhfgshdhjdsgfjsdgfgdsjgfjsd");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-14" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).sendKeys("123123123213213213728163872163872168321");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-15" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).sendKeys("!#$%$#%#$%$#%$#%#$%#$%#$%#$%#$");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-16" + ".png"));
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).clear();
		d.findElement(By.xpath("//*[@id=\"mat-input-3\"]")).sendKeys("3022854498");
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-17" + ".png"));
		// Terminos y Condiciones
		d.findElement(By.xpath(
				"/html/body/app-root/app-consult-expiration/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[4]/div[1]/app-expiration-form/form/div[5]/button"))
				.click();
		d.findElement(By.xpath("//*[@id=\"mat-slide-toggle-1\"]/label/div")).click();
		d.findElement(By.xpath(
				"/html/body/app-root/app-consult-expiration/app-sidenav-menu/mat-sidenav-container/mat-sidenav-content/div/div[4]/div[1]/app-expiration-form/form/div[5]/button"))
				.click();
		d.manage().timeouts().implicitlyWait(120, TimeUnit.SECONDS);
		FileUtils.copyFile(capturaP, new File(url + "Consulta-18" + ".png"));
		
	}

}
