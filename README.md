# Prueba - GR5

Se uso el IDE de "Eclipse-workspace".

Tener en cuenta descargar lo siguiente:
	- Descargar la librería selenium.java.3.13 o actuales.
	- Descargar chromedriver.

Agregar los Jars de Selenium al proyecto.
 
Modificar siguientes rutas las cuales de igual manera se encuentran "Indicadas" en el código:

	- La ruta indicada en la variable url (Esta es la ruta donde se guardan las evidencias). 
	- Modificar la ruta del webdriver (Esta debe ser indicando la ubicación del archivo "chromedriver.exe")

Las clases están listas para ejecutar.

Ejecutar las siguientes clases:

1. ConsultaBeneficios
2. CosultaSoat
3. PagarSoat